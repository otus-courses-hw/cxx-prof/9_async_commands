#include <mutex>
#include <stdexcept>
#include <utility>

#include "app.hpp"
#include "async.h"

#include <boost/uuid/random_generator.hpp>

App::App(std::size_t size) :
    inner_block_cmd{0},
    m_handler(new ProcessCmdStatic()),
    m_buff(new Buffer<Container<Command>>(size)),
    m_writer(nullptr),
    m_reader(nullptr)
{
}

void App::set_writer(IWriterPtr writer)
{
    m_writer = writer;
}

void App::set_reader(IReaderPtr reader)
{
    m_reader = reader;
}

void App::add_cmd(std::string cmd)
{
    m_buff->add(cmd);
    m_handler->add_cmd(this);
}

void App::add_block()
{
    m_handler->add_block(this);
}

void App::del_block()
{
    m_handler->del_block(this);
}

void App::process_cmd()
{
    auto when_block_started = m_buff->time();
    m_writer->write(m_buff->cbegin(), m_buff->cend(), when_block_started);
    m_buff->clear();
}

void App::end()
{
    m_handler->end(this);
    m_writer->stop();
    exit(1);
}

App::UUID App::connect(std::size_t bulk_size)
{
    auto handle = async::connect(bulk_size);

    boost::uuids::random_generator gen;
    UUID uuid = gen();

    std::lock_guard lk(m_mutex);
    contexts.emplace(std::make_pair(uuid, handle));

    return uuid;
}

void App::receive(const UUID &uuid, const char* data)
{
    auto handle = contexts.at(uuid);
    async::receive(handle, data, strlen(data));
}

void App::disconnect(const UUID &uuid)
{
    auto handle = contexts.at(uuid);

    auto bulk = async::context_bulk(handle);
    m_buff->set_threshold(bulk);

    auto data = async::context_stream(handle);
    std::istringstream iss(data);
    m_reader->read(iss);

    async::disconnect(handle);

    std::lock_guard lk(m_mutex);
    contexts.erase(uuid);
}
