#ifndef APP_HPP
#define APP_HPP

#include <bits/c++config.h>
#include <stdexcept>
#include <string>
#include <vector>
#include <unordered_map>
#include <memory>
#include <mutex>

#include "writer.hpp"
#include "reader.hpp"
#include "command_handler.hpp"
#include "buffer.hpp"
#include "command.hpp"
#include "async.h"

#include <boost/uuid/uuid.hpp>
#include <boost/functional/hash.hpp>

class App
{
    public:
        App() = delete;
        App(std::size_t);

        friend struct ProcessCmdStatic;
        friend struct ProcessCmdDynamic;

        template <typename T>
        using Container = std::vector<T>;

        using BuffPtr = std::shared_ptr<BufferBase<Container<Command>>>;

        using BuffIter = Container<Command>::const_iterator;
        
        using IWriterPtr = std::shared_ptr<IWriter<BuffIter>>;

        using IReaderPtr = std::shared_ptr<IReader>;
        
        using ICmdUserHandlerPtr = std::unique_ptr<ICmdUserHandler>;

        using UUID = boost::uuids::uuid;

        void set_writer(IWriterPtr);
        void set_reader(IReaderPtr);
        void add_cmd(std::string);
        void add_block();
        void del_block();
        void end() noexcept(false);
        void process_cmd();

        UUID connect(std::size_t);
        void receive(const UUID&, const char *);
        void disconnect(const UUID&);

    private:
        std::size_t inner_block_cmd;
        ICmdUserHandlerPtr m_handler;
        BuffPtr m_buff;
        IWriterPtr m_writer;
        IReaderPtr m_reader;
        std::unordered_map<UUID, async::handle_t, boost::hash<UUID> > contexts;
        std::mutex m_mutex;
};

#endif /*APP_HPP*/
