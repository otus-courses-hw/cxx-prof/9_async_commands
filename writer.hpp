#pragma once

#include <bits/c++config.h>
#include <deque>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iterator>
#include <algorithm>
#include <string_view>
#include <chrono>
#include <list>
#include <memory>
#include <condition_variable>
#include <mutex>
#include <thread>
#include <queue>
#include <atomic>

#include "command.hpp"

using TimePoint = std::chrono::time_point<std::chrono::system_clock>;

template <typename Iter>
struct IWriter
{
    virtual void write(Iter, Iter, TimePoint) = 0;
    virtual void stop() = 0;
};

template <typename Iter>
class BaseWriter : public IWriter<Iter>
{
    public:
        using Block = std::pair<TimePoint, std::deque<Command> >;

        BaseWriter() = default;
        BaseWriter(std::size_t);
        ~BaseWriter();

        virtual void write(Iter, Iter, TimePoint) override;
        virtual void stop() override;

    protected:
        std::condition_variable_any m_cv;
        std::mutex m_mutex;
        std::list<std::thread> m_consumers;
        std::queue<Block> m_qu;
        std::atomic<bool> m_finished;

        virtual void consumer();
};

template <typename Iter>
class CliWriter : public BaseWriter<Iter>
{
    public:
        CliWriter() = delete;
        CliWriter(std::size_t);

    private:
        void consumer() override;

};

template <typename Iter>
class FileWriter : public BaseWriter<Iter>
{
    public:
        FileWriter() = delete;
        FileWriter(std::size_t);

    private:
        void consumer() override;
};

template <typename Iter>
class MultiWriter : public BaseWriter<Iter>
{
    public:
        using IWriterPtr = std::unique_ptr<IWriter<Iter>>;

        MultiWriter() = delete;

        template <class U>
        MultiWriter(U u)
        {
            m_writers.push_back(std::move(u));
        }

        template <class T, class ...Ts>
        MultiWriter(T t, Ts... ts) : MultiWriter(std::forward<Ts>(ts)...)
        {
            m_writers.push_back(std::move(t));
        }

        void write(Iter, Iter, TimePoint) override;

        void stop() override;

    private:
        std::list<IWriterPtr> m_writers;
};

template <typename Iter>
BaseWriter<Iter>::BaseWriter(std::size_t threads) :
    m_finished{false}
{
    while (threads-- > 0)
        m_consumers.emplace_back(&BaseWriter::consumer, this);
}

template <typename Iter>
BaseWriter<Iter>::~BaseWriter()
{
    this->stop();
}

template <typename Iter>
void BaseWriter<Iter>::write(Iter begin, Iter end, TimePoint tp)
{
    if (begin != end)
    {
        std::lock_guard lk(m_mutex);

        std::deque<Command> commands(begin, end);
        m_qu.emplace(tp, commands);

        m_cv.notify_all();
    }
}

template <typename Iter>
void BaseWriter<Iter>::stop()
{
    m_finished.store(true);
    m_cv.notify_all();

    for(auto &consumer : m_consumers)
    {
        if (consumer.joinable())
            consumer.join();
    }
}

template <typename Iter>
void BaseWriter<Iter>::consumer()
{
}

template <typename Iter>
CliWriter<Iter>::CliWriter(std::size_t threads) :
    BaseWriter<Iter>::BaseWriter(threads)
{
}

template <typename Iter>
void CliWriter<Iter>::consumer()
{
    while(!BaseWriter<Iter>::m_finished)
    {
        std::unique_lock lk(BaseWriter<Iter>::m_mutex);

        while(!BaseWriter<Iter>::m_finished && BaseWriter<Iter>::m_qu.empty())
            BaseWriter<Iter>::m_cv.wait(lk);

        if(BaseWriter<Iter>::m_finished) break;

        std::deque<Command> cmds;
        if (!BaseWriter<Iter>::m_qu.empty())
        {
            auto val = BaseWriter<Iter>::m_qu.front();
            BaseWriter<Iter>::m_qu.pop();

            cmds = val.second;
        }
        lk.unlock();

        std::cout << "bulk: ";
        std::copy(cmds.begin(), cmds.end() - 1, std::ostream_iterator<std::string>(std::cout, ", "));
        std::copy(cmds.end() - 1, cmds.end(), std::ostream_iterator<std::string>(std::cout, " "));
        std::cout << std::endl;
    }
}

template <typename Iter>
FileWriter<Iter>::FileWriter(std::size_t threads) :
    BaseWriter<Iter>::BaseWriter(threads)
{
}

template <typename Iter>
void FileWriter<Iter>::consumer()
{
    std::stringstream ss;
    ss << std::this_thread::get_id();
    std::string thread_postfix = ss.str().substr(11, 4);

    while(!BaseWriter<Iter>::m_finished)
    {
        std::unique_lock lk(BaseWriter<Iter>::m_mutex);

        while(!BaseWriter<Iter>::m_finished && BaseWriter<Iter>::m_qu.empty())
            BaseWriter<Iter>::m_cv.wait(lk);

        if(BaseWriter<Iter>::m_finished) break;

        TimePoint tp;
        std::deque<Command> cmds;
        if (!BaseWriter<Iter>::m_qu.empty())
        {
            auto val = BaseWriter<Iter>::m_qu.front();
            BaseWriter<Iter>::m_qu.pop();

            tp = val.first;
            cmds = val.second;
        }
        lk.unlock();

        auto epoch_time = std::chrono::duration_cast<std::chrono::seconds>(
                tp.time_since_epoch()).count();

        std::string filename = "bulk" + std::to_string(epoch_time) + ".log." + thread_postfix;
        std::ofstream ofs{filename};

        std::copy(cmds.begin(), cmds.end(), std::ostream_iterator<std::string_view>(ofs, "\n"));
    }
}

template <typename Iter>
void MultiWriter<Iter>::write(Iter begin, Iter end, TimePoint tp)
{
    for (const auto &writer : m_writers)
        writer->write(begin, end, tp);
}

template <typename Iter>
void MultiWriter<Iter>::stop()
{
    for (const auto &writer : m_writers)
        writer->stop();
}
