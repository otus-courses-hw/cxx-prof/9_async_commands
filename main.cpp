#include "app.hpp"
#include "reader.hpp"
#include "writer.hpp"
#include <chrono>
#include <cstring>
#include <exception>
#include <memory>
#include <stdexcept>
#include <thread>

int main()
{
    std::shared_ptr<App> app(new App(3));

    using iter_type = typename App::BuffIter;
    std::shared_ptr<IWriter<iter_type>> writer(new MultiWriter<iter_type>(
                std::make_unique<CliWriter<iter_type>>(1),
                std::make_unique<FileWriter<iter_type>>(2)
                ));

    app->set_writer(writer);

    std::shared_ptr<IReader> reader(new StreamReader(app));
    app->set_reader(reader);

    auto handle1 = app->connect(3);
    auto handle2 = app->connect(2);

    app->receive(handle1, "cmd for client 1\nlast cmd before inner block\n{\nclient 1 start to form dynamic block\n{\ncl1 cmd1\n");
    app->receive(handle2, "cmd for client 2\nlast cmd from client 2 which fit to buffer size\nunfortuanately cmd is missed if client disconet before fitting buffer's size");
    app->receive(handle1, "client 1 continuing to fill inner block\ncl1 cmd2\n{\ncmd21\ncmd22\n}\ncmd3\n}\nclient 1 stop to send commands and disconnected\n}");

    app->disconnect(handle1);
    app->disconnect(handle2);

    std::this_thread::sleep_for(std::chrono::seconds(1));

    return 0;
}
