#pragma once

#include <bits/c++config.h>
#include <sstream>
#include <string>

namespace async
{
    using handle_t = void *;
    
    handle_t connect(std::size_t bulk);
    void receive(handle_t handle, const char *data, std::size_t size);
    void disconnect(handle_t handle);
    std::size_t context_bulk(handle_t);
    std::string context_stream(handle_t);

}
