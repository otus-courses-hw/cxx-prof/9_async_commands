#include "async.h"

#include <bits/c++config.h>
#include <iterator>
#include <algorithm>
#include <sstream>

namespace async {

    namespace{
        struct context
        {
            context(std::size_t);

            const std::size_t m_bulk;
            std::stringstream m_stream;
        };

        context::context(std::size_t bulk) :
            m_bulk(bulk),
            m_stream()
        {
        }
    }

    handle_t connect(std::size_t bulk)
    {
        return static_cast<handle_t>(new context(bulk));
    }
    
    void receive(handle_t handle, const char *data, std::size_t size)
    {
        auto ctx = static_cast<context*>(handle);

        std::copy(data,
                data + size,
                std::ostream_iterator<char>(ctx->m_stream, "")
                );
    }
    
    void disconnect(handle_t handle)
    {
        auto ctx = static_cast<context*>(handle);

        delete ctx;
    }

    std::size_t context_bulk(handle_t handle)
    {
        auto ctx = static_cast<context*>(handle);
        return ctx->m_bulk;
    }

    std::string context_stream(handle_t handle)
    {
        auto ctx = static_cast<context*>(handle);
        return ctx->m_stream.str();
    }

}
